// burger menu // 

const burgerOpen = document.querySelector('.burger__button');
const navMenu = document.querySelector('.nav__menu');
const menuClose = document.querySelector('.menu__button');
const overlay = document.querySelector('.overlay')

const openMenu = function() {
    navMenu.classList.toggle('nav__menu--show');
    overlay.classList.toggle('overlay--active');
};

const closeMenu = function() {
    navMenu.classList.remove('nav__menu--show');
    overlay.classList.remove('overlay--active')
}

burgerOpen.addEventListener('click', function () {
    openMenu();
})

menuClose.addEventListener('click', function () {
    closeMenu();
})


