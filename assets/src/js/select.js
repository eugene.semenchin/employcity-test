// select // 

const selectElements = document.querySelectorAll('.select__custom');
const onCloseSelect = (control, target) => {
    const didClickedOutside = !control.contains(target);
    if (didClickedOutside) {
        control.classList.remove('select--active');
    }
};

const onOpenSelect = (control) => control.classList.toggle("select--active");
const onChangeSelect = (option, selectControl) => {
    const triggelControl = selectControl.querySelector('.select__trigger');
    triggelControl.textContent = option.textContent;
    onCloseSelect(selectControl);
}

document.addEventListener("click", (evt) => {
    selectElements.forEach((el) => onCloseSelect(el, evt.target));
});

selectElements.forEach((el) => {
    const selectControl = el;
    const optionsList = selectControl.querySelectorAll('.option');
    const triggelControl = selectControl.querySelector('.select__trigger');

    triggelControl.addEventListener("click", () => onOpenSelect(selectControl));
    optionsList.forEach((option) => {
        option.addEventListener("click", () => onChangeSelect(option, selectControl));
    })
});