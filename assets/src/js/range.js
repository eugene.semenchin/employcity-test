// range //

const rangeSlider = document.querySelector('.slider__range');
const rangeValue = document.querySelector('.slider__value');
rangeValue.innerHTML = rangeSlider.value;

rangeSlider.oninput = function() {
    rangeValue.innerHTML = this.value;
}
  